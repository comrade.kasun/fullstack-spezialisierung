import Vue from 'vue'
import App from './App.vue'
import axios from 'axios'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import './assets/reset.css'
import store from './store'
import VueAxios from 'vue-axios'
import router from './router'

Vue.config.productionTip = false
Vue.use(VueAxios, axios)

new Vue({
  router, store,
  render: h => h(App),
}).$mount('#app')
