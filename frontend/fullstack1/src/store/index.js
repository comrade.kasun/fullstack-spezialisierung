import Vuex from 'vuex';
import Vue from 'vue';
import vStylish from 'v-stylish';


//Load Vues
Vue.use(Vuex);
Vue.use(vStylish);

//Create store
export default new Vuex.Store({
    state: {
        alleTrainerInnen: [],
        trainerInnenById:'',
        alleGruppen: [],
        gruppeById: '',
        alleRaeume: [],
        raumById: '',
        alleTeilnehmerInnen: [],
        teilnehmerInnenById: '',

    },

    mutations: {

        //TrainerInnen

        unshiftAlleTrainerInnen(state, neueTrainerIn){
            state.alleTrainerInnen.unshift(neueTrainerIn)
        },
        
        setAlleTrainerInnen(state, alleTrainerInnen){
            state.alleTrainerInnen = alleTrainerInnen;
        },

        
        setTrainerInnenById(state, trainerInnenById) {
            state.trainerInnenById = trainerInnenById;
        },

        //TeilnehmerInnen

        setAlleTeilnehmerInnen(state, alleTeilnehmerInnen){
            state.alleTeilnehmerInnen = alleTeilnehmerInnen;
        },

        unshiftAlleTeilnehmerInnen(state, neueTeilnehmerIn){
            state.alleTeilnehmerInnen.unshift(neueTeilnehmerIn)
        },
        setTeilnehmerInnenById(state, teilnehmerInnenById) {
            state.teilnehmerInnenById = teilnehmerInnenById;
        },

        //Gruppe

        setAlleGruppen(state, alleGruppen){
            state.alleGruppen = alleGruppen;
        },
        setGruppeById(state, gruppeById) {
            state.gruppeById = gruppeById;
        },
        setAlleRaeume(state, alleRaeume){
            state.alleRaeume = alleRaeume;
        },
        setRaumById(state, raumById) {
            state.raumById = raumById;
        },
    },

    actions: {
        async getAlleTrainerInnen(context) {
            const response = await Vue.axios.get('http://localhost:8080/trainerInnen/')
            console.log(response)
            context.commit('setAlleTrainerInnen', response.data)
        },

        async getTrainerInnenById(context, id) {
            const response = await Vue.axios.get('http://localhost:8080/trainerIn/'+id)
            console.log(response)
        },

        async deleteTrainerIn(context, id) {
            await Vue.axios.delete('http://localhost:8080/trainerIn/'+id);
            context.dispatch('getAlleTrainerInnen');
        },

        async putTrainerIn(context, trainerInnenUpdate) {
            const response = await Vue.axios.put('http://localhost:8080/trainerIn/' + trainerInnenUpdate.id, trainerInnenUpdate);
            console.log(response);
            context.dispatch('getAlleTrainerInnen');
        },

        async postTrainerIn(context, trainerInnenUpdate){
            await Vue.axios.post('http://localhost:8080/trainerInnen/', trainerInnenUpdate)

        },

        //TeilnehmerInnen

        async getAlleTeilnehmerInnen(context) {
            const response = await Vue.axios.get('http://localhost:8080/teilnehmerInnen+gruppennamen/')
            console.log(response)
            context.commit('setAlleTeilnehmerInnen', response.data)
        },

        async getTeilnehmerInnenById(context, id) {
            const response = await Vue.axios.get('http://localhost:8080/teilnehmerIn+gruppenname/'+id)
            console.log(response)
        },

        async deleteTeilnehmerIn(context, id) {
            await Vue.axios.delete('http://localhost:8080/teilnehmerIn+gruppenname/'+id);
            context.dispatch('getAlleTeilnehmerInnen');
        },

        async putTeilnehmerIn(context, teilnehmerInnenUpdate) {
            const response = await Vue.axios.put('http://localhost:8080/teilnehmerIn+gruppenname/' + teilnehmerInnenUpdate.teilnehmerId, teilnehmerInnenUpdate);
            console.log(response);
            context.dispatch('getAlleTeilnehmerInnen');
        },

        async postTeilnehmerIn(context, teilnehmerInnenUpdate){
            console.log(teilnehmerInnenUpdate)
            await Vue.axios.post('http://localhost:8080/teilnehmerIn+gruppenname/', teilnehmerInnenUpdate)
        },

        // Gruppen

        async getAlleGruppen(context) {
            const response = await Vue.axios.get('http://localhost:8080/gruppenteilnehmerDto/')
            console.log(response)
            context.commit('setAlleGruppen', response.data)
        },

        async getGruppeById(context, id) {
            const response = await Vue.axios.get('http://localhost:8080/gruppe/'+id)
            console.log(response)
        },
        async deleteGruppe(context, id) {
            await Vue.axios.delete('http://localhost:8080/gruppe/'+id);
            context.dispatch('getAlleGruppen');
        },

        async putGruppe(context, gruppenUpdate) {
            const response = await Vue.axios.put('http://localhost:8080/gruppenteilnehmerDto/' + gruppenUpdate.id, gruppenUpdate);
            console.log(response);
            context.dispatch('getAlleGruppen');
        },

        async postGruppe(context, gruppenUpdate){
            await Vue.axios.post('http://localhost:8080/gruppe/', gruppenUpdate)
            context.dispatch('getAlleGruppen')
        },

        // Räume
        // async getAlleRaeume(context) {
        //     const response = await Vue.axios.get('http://localhost:8080/raeume/')
        //     console.log(response)
        //     context.commit('setAlleRaeume', response.data)
        // },

        // async getRaumById(context, id) {
        //     const response = await Vue.axios.get('http://localhost:8080/raum/'+id)
        //     console.log(response)
        // },
        // async deleteRaum(context, id) {
        //     await Vue.axios.delete('http://localhost:8080/raum/'+id);
        //     context.dispatch('getAlleRaeume');
        // },

        // async putRaum(context, raumUpdate) {
        //     const response = await Vue.axios.put('http://localhost:8080/raum/' + raumUpdate.id, raumUpdate);
        //     console.log(response);
        //     context.dispatch('getAlleRaeume');
        // },

        // async postRaum(context, raumUpdate){
        //     await Vue.axios.post('http://localhost:8080/raum/', raumUpdate)
        //     context.dispatch('getAlleRaeume')
        // },

        // async postTrainerIn(context, trainerIn){
        //     await Vue.axios.post('')
        // }

    },
    
    
});

