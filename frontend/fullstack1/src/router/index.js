import Vue from 'vue'
import VueRouter from 'vue-router'
import UebersichtPage from '../components/UebersichtPage.vue'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'UebersichtPage',
        component: UebersichtPage
    },
    {
        path: '/trainerInnenPage',
        name: 'TrainerInnenPage',
        component: () => import('../components/TrainerInnenPage.vue')
    },
    {
        path: '/teilnehmerInnen',
        name: 'TeilnehmerInnenPage',
        component: () => import('../components/TeilnehmerInnenPage.vue')
    },
    {
        path: '/gruppen',
        name: 'GruppenPage',
        component: () => import('../components/GruppenPage.vue')
    }
]

const router = new VueRouter({
    routes
})

export default router