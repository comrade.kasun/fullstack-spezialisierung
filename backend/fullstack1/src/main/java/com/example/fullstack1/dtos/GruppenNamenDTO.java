package com.example.fullstack1.dtos;

import com.example.fullstack1.enums.KursEnde;
import com.example.fullstack1.enums.KursStart;
import com.example.fullstack1.model.Gruppe;
import com.example.fullstack1.model.TeilnehmerIn;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GruppenNamenDTO {
    private int id;
    private String gruppenname;

    @JsonCreator
    public GruppenNamenDTO(@JsonProperty("id") int id, @JsonProperty("gruppenname") String gruppenname) {
        this.id = id;
        this.gruppenname = gruppenname;
    }

    public GruppenNamenDTO() {

    }

    public GruppenNamenDTO(Gruppe gruppe) {
        this.id = gruppe.getId();
        this.gruppenname = gruppe.getGruppenname();
    }
}
