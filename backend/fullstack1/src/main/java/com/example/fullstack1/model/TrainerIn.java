package com.example.fullstack1.model;

import com.example.fullstack1.enums.Beschaeftigung;
import com.example.fullstack1.enums.Fachwissen;
import com.example.fullstack1.enums.Verfuegbarkeit;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity(name= "trainerIn")
public class TrainerIn extends Person {

    @Column
    private String geburtstag;

    @Column
    private String adresse;

    @Column
    private String telefon;

    @Column
    private Beschaeftigung beschaeftigung;

    @Column
    private Fachwissen fachwissen;

    /*@Column
    @ElementCollection
    private Set<Verfuegbarkeit> verfuegbarkeit;*/

    @Column
    private Verfuegbarkeit verfuegbarkeit;

    @Column
    private double bruttolohn;

    @Column
    private double nettolohn;

    @OneToMany(mappedBy = "trainerIn")
    @JsonIgnoreProperties({"trainerIn"})
    private Set<Unterricht> unterrichtSet = new HashSet<>();

    public TrainerIn() {}

    public TrainerIn(String name, String emailAdresse, String geburtstag, String adresse, String telefon, Beschaeftigung beschaeftigung, Fachwissen fachwissen, Verfuegbarkeit verfuegbarkeit, double bruttolohn, double nettolohn) {
        super(name, emailAdresse);
        this.geburtstag = geburtstag;
        this.adresse = adresse;
        this.telefon = telefon;
        this.beschaeftigung = beschaeftigung;
        this.fachwissen = fachwissen;
        this.verfuegbarkeit = verfuegbarkeit;
        this.bruttolohn = bruttolohn;
        this.nettolohn = nettolohn;
    }

    //getters & setters


    public double getNettolohn() {
        return nettolohn;
    }

    public void setNettolohn(double nettolohn) {
        this.nettolohn = nettolohn;
    }

    public String getGeburtstag() {
        return geburtstag;
    }

    public void setGeburtstag(String geburtstag) {
        this.geburtstag = geburtstag;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public Beschaeftigung getBeschaeftigung() {
        return beschaeftigung;
    }

    public void setBeschaeftigung(Beschaeftigung beschaeftigung) {
        this.beschaeftigung = beschaeftigung;
    }

    public Fachwissen getFachwissen() {
        return fachwissen;
    }

    public void setFachwissen(Fachwissen fachwissen) {
        this.fachwissen = fachwissen;
    }

    public Verfuegbarkeit getVerfuegbarkeit() {
        return verfuegbarkeit;
    }

    public void setVerfuegbarkeit(Verfuegbarkeit verfuegbarkeit) {
        this.verfuegbarkeit = verfuegbarkeit;
    }

    public double getBruttolohn() {
        return bruttolohn;
    }

    public void setBruttolohn(double bruttolohn) {
        this.bruttolohn = bruttolohn;
    }

    public Set<Unterricht> getUnterrichtSet() {
        return unterrichtSet;
    }

    public void setUnterrichtSet(Unterricht unterricht) {
        this.unterrichtSet = unterrichtSet;
    }


}
