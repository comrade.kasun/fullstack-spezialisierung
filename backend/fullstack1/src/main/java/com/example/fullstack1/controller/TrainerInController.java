package com.example.fullstack1.controller;

import com.example.fullstack1.dtos.TrainerInDtoAll;
import com.example.fullstack1.dtos.TrainerInDtoName;
import com.example.fullstack1.model.TrainerIn;
import com.example.fullstack1.service.TrainerInService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TrainerInController {

    @Autowired
    TrainerInService trainerInService;

    // GET

    // Get alle TrainerInnen
    @CrossOrigin
    @GetMapping("/trainerInnen")
    public List<TrainerIn> getTrainerInnen() {
        return trainerInService.getTrainerInnen();
    }

    // Get TrainerInnen per ID
    @CrossOrigin
    @GetMapping("/trainerIn/{trainerId}")
    public TrainerIn getTrainerInById(@PathVariable int trainerId) {
        return trainerInService.getTrainerInnen(trainerId);
    }

    //Put TrainerIn per ID
    @CrossOrigin
    @PutMapping("/trainerIn/{trainerId}")
    public void putTrainer(@RequestBody TrainerIn trainerIn, @PathVariable int trainerId) {
        trainerInService.put(trainerIn, trainerId);
    }

    //Post TrainerIn
    @CrossOrigin
    @PostMapping("trainerInnen")
    public void postTrainer(@RequestBody TrainerIn trainerIn) {
        trainerInService.post(trainerIn);
    }

    //Delete TrainerIn nach ID
    @CrossOrigin
    @DeleteMapping("/trainerIn/{trainerId}")
    public void deleteTrainer(@PathVariable int trainerId) {
        trainerInService.delete(trainerId);
    }


    //DTO

    // Get alle TrainerInnen-Daten DTO
    @CrossOrigin
    @GetMapping("/trainerInnenDaten")
    public List<TrainerInDtoAll> getTrainerInnenDaten(){return trainerInService.getTrainerInnenDaten();}

    // Get TrainerInnen-Namen DTO
    @CrossOrigin
    @GetMapping("/trainerInnenNamen")
    public List<TrainerInDtoName> getTrainerInnenNamen(){return trainerInService.getTrainerInnenNamen();}


    //Put TrainerIn-NameDTO
    @CrossOrigin
    @PutMapping(value = "trainerInName/{trainerId}", consumes = "application/json")
    public void putTrainer(@PathVariable int trainerId, @RequestBody TrainerInDtoName trainerInDtoName){
        trainerInService.put(trainerId, trainerInDtoName);
    }
  }
