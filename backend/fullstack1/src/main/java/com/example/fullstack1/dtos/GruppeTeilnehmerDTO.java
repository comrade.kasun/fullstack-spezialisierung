package com.example.fullstack1.dtos;
import com.example.fullstack1.enums.KursKategorie;
import com.example.fullstack1.enums.RaumName;
import com.example.fullstack1.model.Gruppe;
import com.example.fullstack1.model.TeilnehmerIn;
import lombok.Data;

import java.util.HashSet;
import java.util.Set;

@Data
public class GruppeTeilnehmerDTO {

     private RaumName raumname;

     private boolean qualifying;

     private KursKategorie kursKategorie;

     private String gruppenname;

     private Set<TeilnehmerInnenNamenDto> teilnehmerNamenSet = new HashSet<>();

     public GruppeTeilnehmerDTO(RaumName raumname, boolean qualifying, KursKategorie kursKategorie, String gruppenname, Set<TeilnehmerInnenNamenDto> teilnehmerNamenSet) {
          this.raumname = raumname;
          this.qualifying = qualifying;
          this.kursKategorie = kursKategorie;
          this.gruppenname = gruppenname;
          this.teilnehmerNamenSet = teilnehmerNamenSet;
     }

     public GruppeTeilnehmerDTO(Gruppe gruppe) {
          this.raumname = gruppe.getRaumName();
          this.qualifying = gruppe.isQualifying();
          this.kursKategorie = gruppe.getKursKategorie();
          this.gruppenname = gruppe.getGruppenname();
          gruppe.getTeilnehmerSet().forEach(t -> this.teilnehmerNamenSet.add(new TeilnehmerInnenNamenDto(t.getId(),t.getName())));
     }
}
