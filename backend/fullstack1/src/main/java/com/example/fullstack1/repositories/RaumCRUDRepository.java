package com.example.fullstack1.repositories;

import com.example.fullstack1.model.Raum;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RaumCRUDRepository extends CrudRepository<Raum, Integer> {
}
