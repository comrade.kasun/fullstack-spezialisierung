package com.example.fullstack1.enums;

public enum KursStart {
    JÄNNER, FEBRUAR, MÄRZ, APRIL, MAI, JUNI, JULI, AUGUST, SEPTEMBER, OKTOBER, NOVEMBER, DEZEMBER
}
