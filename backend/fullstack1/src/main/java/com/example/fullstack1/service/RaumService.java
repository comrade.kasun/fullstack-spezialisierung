package com.example.fullstack1.service;

import com.example.fullstack1.model.Raum;
import com.example.fullstack1.repositories.RaumCRUDRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RaumService {

    RaumCRUDRepository raumCRUDRepository;

    @Autowired
    public RaumService(RaumCRUDRepository raumCRUDRepository) {
        this.raumCRUDRepository = raumCRUDRepository;
    }

    //Get alle Räume
    public List<Raum> getRaeume() {
        return (List<Raum>) raumCRUDRepository.findAll();
    }

    //Get Raum per ID
    public Raum getRaumById(int raumId) {
        return (Raum) raumCRUDRepository.findById(raumId).get();
    }

    //Put Raum per ID
    public void put(Raum raum, Integer raumId) {
        Raum raumDb = raumCRUDRepository.findById(raumId).get();

        if(raum.getRaumName() != null)
            raumDb.setRaumName(raum.getRaumName());
        if (raum.getRaumBeschreibung() != null)
            raumDb.setRaumBeschreibung(raum.getRaumBeschreibung());

        raumCRUDRepository.save(raumDb);
    }

    //Post Raum
    public void post(Raum raum) {
        raumCRUDRepository.save(raum);
    }

    //Delete Raum
    public void delete(int raumId) {
        raumCRUDRepository.deleteById(raumId);
    }
}
