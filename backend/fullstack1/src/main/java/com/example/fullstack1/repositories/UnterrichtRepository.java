//package com.example.fullstack1.repositories;
//
//import com.example.fullstack1.model.Gruppe;
//import com.example.fullstack1.model.TeilnehmerIn;
//import com.example.fullstack1.model.TrainerIn;
//import com.example.fullstack1.model.Unterricht;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Repository;
//
//import javax.transaction.Transactional;
//
//@Repository
//@Transactional
//public class UnterrichtRepository {
//
//    @Autowired
//    UnterrichtCRUDRepository unterrichtCRUDRepository;
//
//    @Autowired
//    GruppeCRUDRepository gruppeCRUDRepository;
//
//
//
//    //erforderlich bei einer aufgesplitteten ManyToMany Beziehung
//    public void setUnterricht(Integer gruppeId) {
//        //Objekte aus Datenbank holen
////        TrainerIn trainerIn = trainerInCRUDRepository.findById(trainerId).get();
//        Gruppe gruppe = gruppeCRUDRepository.findById(gruppeId).get();
//
//
//        //neuen Unterricht erstellen
////        Unterricht unterricht = new Unterricht(trainerIn);
//        Unterricht unterricht = new Unterricht(gruppe);
//
//        //Objekte updaten im Backend
////        trainerIn.setUnterrichtSet(unterricht);
//        gruppe.setUnterrichtSet(unterricht);
//
//        //DB updaten
//        unterrichtCRUDRepository.save(unterricht);
////        trainerInCRUDRepository.save(trainerIn);
//        gruppeCRUDRepository.save(gruppe);
//    }
//
//}
