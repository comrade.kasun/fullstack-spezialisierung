package com.example.fullstack1.repositories;

import com.example.fullstack1.model.TrainerIn;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TrainerInCRUDRepository extends JpaRepository<TrainerIn, Integer> {
//    List<TrainerIn> findByName(String name);
//    TrainerIn findOneByName(String name);
//    List<TrainerIn> findByNameIsNot(String name);
//    List<TrainerIn> findByNameIsNull();
//    List<TrainerIn> findByNameIsNotNull();
//    List<TrainerIn> findByNameStartingWith(String prefix);
//    List<TrainerIn> findByNameEndingWith(String suffix);
//    List<TrainerIn> findByNameContaining(String infix);
//    List<TrainerIn> findByNameOrderByName(String name);
//    List<TrainerIn> findByNameOrderByNameDesc(String name);
//    List<TrainerIn> findByNameOrGeburtstag(String name, String geburtstag);
}


