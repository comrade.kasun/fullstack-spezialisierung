package com.example.fullstack1.enums;

public enum KursKategorie {
    SW("Software"),
    NT("Netzwerktechnik"),
    FIA("FIA");

    private final String name;

    KursKategorie(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }
}
