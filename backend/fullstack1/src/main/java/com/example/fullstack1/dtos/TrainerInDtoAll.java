package com.example.fullstack1.dtos;

import com.example.fullstack1.enums.Beschaeftigung;
import com.example.fullstack1.enums.Fachwissen;
import com.example.fullstack1.enums.Verfuegbarkeit;

public class TrainerInDtoAll {
    private int id;
    private String name;
    private String emailAdresse;
    private String geburtstag;
    private String adresse;
    private String telefon;
    private Beschaeftigung beschaeftigung;
    private Fachwissen fachwissen;
    private Verfuegbarkeit verfuegbarkeit;
    private double bruttolohn;
    private double nettolohn;

    public TrainerInDtoAll(int id, String name, String emailAdresse, String geburtstag, String adresse, String telefon, Beschaeftigung beschaeftigung, Fachwissen fachwissen, Verfuegbarkeit verfuegbarkeit, double bruttolohn, double nettolohn) {
        this.id = id;
        this.name = name;
        this.emailAdresse = emailAdresse;
        this.geburtstag = geburtstag;
        this.adresse = adresse;
        this.telefon = telefon;
        this.beschaeftigung = beschaeftigung;
        this.fachwissen = fachwissen;
        this.verfuegbarkeit = verfuegbarkeit;
        this.bruttolohn = bruttolohn;
        this.nettolohn = nettolohn;
    }

    public TrainerInDtoAll() {

    }

    public double getNettolohn() {
        return nettolohn;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmailAdresse() {
        return emailAdresse;
    }

    public void setEmailAdresse(String emailAdresse) {
        this.emailAdresse = emailAdresse;
    }

    public String getGeburtstag() {
        return geburtstag;
    }

    public void setGeburtstag(String geburtstag) {
        this.geburtstag = geburtstag;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public Beschaeftigung getBeschaeftigung() {
        return beschaeftigung;
    }

    public void setBeschaeftigung(Beschaeftigung beschaeftigung) {
        this.beschaeftigung = beschaeftigung;
    }

    public Fachwissen getFachwissen() {
        return fachwissen;
    }

    public void setFachwissen(Fachwissen fachwissen) {
        this.fachwissen = fachwissen;
    }

    public Verfuegbarkeit getVerfuegbarkeit() {
        return verfuegbarkeit;
    }

    public void setVerfuegbarkeit(Verfuegbarkeit verfuegbarkeit) {
        this.verfuegbarkeit = verfuegbarkeit;
    }

    public double getBruttolohn() {
        return bruttolohn;
    }

    public void setBruttolohn(double bruttolohn) {
        this.bruttolohn = bruttolohn;
    }


    public void setNettolohn(double nettolohn) {
        this.nettolohn = nettolohn;
    }
}
