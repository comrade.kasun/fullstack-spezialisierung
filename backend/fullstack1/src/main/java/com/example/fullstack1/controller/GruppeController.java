package com.example.fullstack1.controller;

import com.example.fullstack1.dtos.GruppeTeilnehmerDTO;
import com.example.fullstack1.model.Gruppe;
import com.example.fullstack1.model.Raum;
import com.example.fullstack1.model.TeilnehmerIn;
import com.example.fullstack1.repositories.GruppeCRUDRepository;
import com.example.fullstack1.repositories.RaumCRUDRepository;
import com.example.fullstack1.repositories.TeilnehmerInCRUDRepository;
import com.example.fullstack1.service.GruppeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class GruppeController {

    @Autowired
    GruppeService gruppeService;

    @Autowired
    TeilnehmerInCRUDRepository teilnehmerInCRUDRepository;

    @Autowired
    GruppeCRUDRepository gruppeCRUDRepository;

    @Autowired
    RaumCRUDRepository raumCRUDRepository;

    //Get alle Gruppen
    @CrossOrigin
    @GetMapping("gruppen")
    public List<Gruppe> getGruppen() {
        return gruppeService.getGruppen();
    }

    //Get Gruppe per ID
    @CrossOrigin
    @GetMapping("/gruppe/{gruppeId}")
    public Gruppe getGruppeById(@PathVariable int gruppeId) {
        return gruppeService.getGruppeById(gruppeId);
    }

    //Put Gruppe per ID
    @CrossOrigin
    @PutMapping("/gruppe/{gruppeId}")
    public void putGruppe(@RequestBody Gruppe gruppe, @PathVariable Integer gruppeId) {
        gruppeService.put(gruppe, gruppeId);
    }

    //Put TeilnehmerIn zu Gruppe
    @CrossOrigin
    @PutMapping("/{gruppeId}/teilnehmerIn/{teilnehmerInId}")
    public void assignTeilnehmerInToGruppe(
            @PathVariable int teilnehmerInId,
            @PathVariable int gruppeId
    ) {
        Gruppe gruppe =  gruppeCRUDRepository.findById(gruppeId).get();
        TeilnehmerIn teilnehmerIn = teilnehmerInCRUDRepository.findById(teilnehmerInId).get();
        gruppe.assignTeilnehmerInToGruppe(teilnehmerIn);
        teilnehmerIn.setGruppe(gruppe);
        gruppeCRUDRepository.save(gruppe);
        teilnehmerInCRUDRepository.save(teilnehmerIn);
    }

    //Put Raum zu Gruppe
//    @CrossOrigin
//    @PutMapping("/{gruppeId}/raum/{raumId}")
//    public void assignRaumToGruppe(
//            @PathVariable int raumId,
//            @PathVariable int gruppeId
//    ) {
//        Gruppe gruppe =  gruppeCRUDRepository.findById(gruppeId).get();
//        Raum raum = raumCRUDRepository.findById(raumId).get();
//        gruppe.assignRaumToGruppe(raum);
//        raum.setGruppe(gruppe);
//        gruppeCRUDRepository.save(gruppe);
//        raumCRUDRepository.save(raum);
//    }

    //Post Gruppe
    @CrossOrigin
    @PostMapping("/gruppe")
    public void postGruppe(@RequestBody Gruppe gruppe) {
        gruppeService.post(gruppe);
    }

    //Delete Gruppe
//    @CrossOrigin
//    @DeleteMapping("/gruppe/{gruppeId}")
//    public void deleteGruppe(@PathVariable int gruppeId) {
//        gruppeService.delete(gruppeId);
//    }

    @CrossOrigin
    @GetMapping("/gruppenteilnehmerDto")
    public List<GruppeTeilnehmerDTO> getGruppeTeilnehmerDaten(){
        return gruppeService.getGruppenTeilnehmerDaten();
    }
}
