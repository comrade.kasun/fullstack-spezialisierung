package com.example.fullstack1.repositories;

import com.example.fullstack1.model.Gruppe;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GruppeCRUDRepository extends CrudRepository<Gruppe, Integer> {
}
