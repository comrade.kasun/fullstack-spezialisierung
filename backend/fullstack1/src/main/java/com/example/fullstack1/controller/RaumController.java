package com.example.fullstack1.controller;

import com.example.fullstack1.model.Raum;
import com.example.fullstack1.service.RaumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class RaumController {

    @Autowired
    RaumService raumService;

    //Get alle Räume
    @CrossOrigin
    @GetMapping("/raeume")
    public List<Raum> getRaeume() {
    return raumService.getRaeume();
    }

    //Get Raum per ID
    @CrossOrigin
    @GetMapping("/raum/{raumId}")
    public Raum getRaumById(@PathVariable int raumId) {
        return raumService.getRaumById(raumId);
    }

    //Put Raum
    @CrossOrigin
    @PutMapping("raum/{raumId}")
    public void putRaum(@RequestBody Raum raum, @PathVariable Integer raumId) {
        raumService.put(raum, raumId);
    }

    //Post Raum per ID
    @CrossOrigin
    @PostMapping("raeume")
    public void postRaum(@RequestBody Raum raum) {
        raumService.post(raum);
    }

    //Delete Raum
    @CrossOrigin
    @DeleteMapping("/raum/{raumId}")
    public void deleteRaum(@PathVariable int raumId) {
        raumService.delete(raumId);
    }

    //DTO DTO DTO DTO DTO


}


