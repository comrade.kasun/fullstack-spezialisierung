package com.example.fullstack1.service;

import com.example.fullstack1.model.Gruppe;
import com.example.fullstack1.model.Unterricht;
import com.example.fullstack1.repositories.GruppeCRUDRepository;
import com.example.fullstack1.repositories.UnterrichtCRUDRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;

@Service
public class UnterrichtService {

    UnterrichtCRUDRepository unterrichtCRUDRepository;

    @Autowired
    public UnterrichtService(UnterrichtCRUDRepository unterrichtCRUDRepository) {
        this.unterrichtCRUDRepository = unterrichtCRUDRepository;
    }

    //Get alle Unterrichtstage
    public List<Unterricht> getUnterrichtstage() {
        return (List<Unterricht>) unterrichtCRUDRepository.findAll();
    }

    //Get Unterricht per ID
    public Unterricht getUnterrichtById(int unterrichtId) {
        return (Unterricht) unterrichtCRUDRepository.findById(unterrichtId).get();
    }

    //Put Unterricht per ID
    public void put(Unterricht unterricht, Integer unterrichtId) {
        Unterricht unterrichtDb = unterrichtCRUDRepository.findById(unterrichtId).get();

        if(unterricht.getFach() != null)
            unterrichtDb.setFach(unterricht.getFach());
        if (unterricht.getKurstag() != null)
            unterrichtDb.setKurstag(unterricht.getKurstag());

        unterrichtCRUDRepository.save(unterrichtDb);
    }


    //Post Unterricht
    public void post(Unterricht unterricht) {
        unterrichtCRUDRepository.save(unterricht);
    }

    //Delete Unterricht
    public void delete(int unterrichtId) {
        unterrichtCRUDRepository.deleteById((unterrichtId));
    }

}
