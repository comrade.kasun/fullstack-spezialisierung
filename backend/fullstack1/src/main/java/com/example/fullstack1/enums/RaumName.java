package com.example.fullstack1.enums;

public enum RaumName {
    ALANTURING("Alan Turing"),
    GRACEHOPPER("Grace Hopper"),
    RADIAPERLMAN("Radia Perlman"),
    LINUSTORVALDS("Linus Torvalds"),
    ADALOVELACE("Ada Lovelace"),
    KONRADZUSE("Konrad Zuse"),
    HEDYLAMARR("Hedy Lamarr"),
    MARGARETHAMILTON("Margaret Hamilton");

    private final String eRaumName;

    RaumName(String eRaumName){
        this.eRaumName = eRaumName;
    }

    public String getERaumName(){
        return eRaumName;
    }
}
