package com.example.fullstack1.dtos;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class TrainerInDtoName {
    private int id;
    private String name;

    @JsonCreator
    public TrainerInDtoName(@JsonProperty("id") int id, @JsonProperty("name") String name) {
        this.id = id;
        this.name = name;
    }

    public TrainerInDtoName() {

    }
}
