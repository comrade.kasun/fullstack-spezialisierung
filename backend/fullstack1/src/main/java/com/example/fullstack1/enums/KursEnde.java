package com.example.fullstack1.enums;

public enum KursEnde {
    JÄNNER, FEBRUAR, MÄRZ, APRIL, MAI, JUNI, JULI, AUGUST, SEPTEMBER, OKTOBER, NOVEMBER, DEZEMBER
}
