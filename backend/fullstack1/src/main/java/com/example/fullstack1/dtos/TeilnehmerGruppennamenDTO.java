package com.example.fullstack1.dtos;

import com.example.fullstack1.enums.KursEnde;
import com.example.fullstack1.enums.KursStart;
import com.example.fullstack1.model.TeilnehmerIn;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TeilnehmerGruppennamenDTO {

    private Integer teilnehmerId;
    private String name;
    private String emailAdresse;
    private KursStart kursStart;
    private KursEnde kursEnde;
    private GruppenNamenDTO gruppe;

    public TeilnehmerGruppennamenDTO(Integer teilnehmerId, String name, String emailAdresse, KursStart kursStart, KursEnde kursEnde, GruppenNamenDTO gruppe) {
        this.teilnehmerId = teilnehmerId;
        this.name = name;
        this.emailAdresse = emailAdresse;
        this.kursStart = kursStart;
        this.kursEnde = kursEnde;
        this.gruppe = gruppe;
    }

    public TeilnehmerGruppennamenDTO(TeilnehmerIn teilnehmerIn) {
        this.teilnehmerId = teilnehmerIn.getId();
        this.name = teilnehmerIn.getName();
        this.emailAdresse = teilnehmerIn.getEmailAdresse();
        this.kursStart = teilnehmerIn.getKursStart();
        this.kursEnde = teilnehmerIn.getKursEnde();
        if (teilnehmerIn.getGruppe() != null) {
            this.gruppe = new GruppenNamenDTO(teilnehmerIn.getGruppe());
        } else {
            this.gruppe = new GruppenNamenDTO(0, "Keine Gruppe");
        }
    }


}
