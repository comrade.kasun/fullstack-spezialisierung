package com.example.fullstack1.repositories;

import com.example.fullstack1.model.TeilnehmerIn;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TeilnehmerInCRUDRepository extends CrudRepository<TeilnehmerIn, Integer> {
}
