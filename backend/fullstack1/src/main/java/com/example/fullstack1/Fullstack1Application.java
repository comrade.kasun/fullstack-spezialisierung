package com.example.fullstack1;

import com.example.fullstack1.enums.*;
import com.example.fullstack1.model.Gruppe;
import com.example.fullstack1.model.TeilnehmerIn;
import com.example.fullstack1.model.TrainerIn;
import com.example.fullstack1.repositories.GruppeCRUDRepository;
import com.example.fullstack1.repositories.TeilnehmerInCRUDRepository;
import com.example.fullstack1.repositories.TrainerInCRUDRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.GregorianCalendar;

@SpringBootApplication
public class Fullstack1Application implements CommandLineRunner {
    public static void main(String[] args) {
        SpringApplication.run(Fullstack1Application.class, args);
    }

    @Autowired
    private GruppeCRUDRepository gruppeCRUDRepository;

    @Autowired
    private TeilnehmerInCRUDRepository teilnehmerInCRUDRepository;

    @Autowired
    private TrainerInCRUDRepository trainerInCRUDRepository;

    @Override
    public void run(String... args) throws Exception {

        for (int i = 0; i < 5; i++) {
            String vorname = "";
            String nachname = "";
            String geburtstag = "";
            String adresse = "";
            String telefon = "1234";
            Beschaeftigung beschaeftigung = Beschaeftigung.valueOf("VOLLZEIT");
            Fachwissen fachwissen = Fachwissen.valueOf("WEB");
            Verfuegbarkeit verfuegbarkeit = Verfuegbarkeit.valueOf("MONTAG");
            double bruttolohn = 2000;
            double nettolohn = 1000;

            //Dummy Daten TrainerInnen-Namen

            int zufallVorname = (int) (Math.random() * 11);
            int zufallNachname = (int) (Math.random() * 11);

            switch (zufallVorname) {
                case 0:
                    vorname = "Emma";
                    break;
                case 1:
                    vorname = "Michael";
                    break;
                case 2:
                    vorname = "Marco";
                    break;
                case 3:
                    vorname = "Sara";
                    break;
                case 4:
                    vorname = "Lisa";
                    break;
                case 5:
                    vorname = "Peter";
                    break;
                case 6:
                    vorname = "Moritz";
                    break;
                case 7:
                    vorname = "Felix";
                    break;
                case 8:
                    vorname = "Susanne";
                    break;
                case 9:
                    vorname = "Mia";
                    break;
                case 10:
                    vorname = "Simon";
                    break;
            }

            switch (zufallNachname) {
                case 0:
                    nachname = "Mustermann";
                    break;
                case 1:
                    nachname = "Musterfrau";
                    break;
                case 2:
                    nachname = "Müller";
                    break;
                case 3:
                    nachname = "Rosenrot";
                    break;
                case 4:
                    nachname = "Bauer";
                    break;
                case 5:
                    nachname = "Weber";
                    break;
                case 6:
                    nachname = "Hoffmann";
                    break;
                case 7:
                    nachname = "Hemmer";
                    break;
                case 8:
                    nachname = "Leithner";
                    break;
                case 9:
                    nachname = "Brunner";
                    break;
                case 10:
                    nachname = "Vogel";
                    break;
            }

            String name = vorname + " " + nachname;
            String emailAdresse = vorname.toLowerCase() + "." + nachname.toLowerCase() + "@outlook.com";


            //Dummy Daten TrainerInnen Geburtstag

            GregorianCalendar gc = new GregorianCalendar();

            int year = randBetween(1980, 2000);

            gc.set(gc.YEAR, year);

            int dayOfYear = randBetween(1, gc.getActualMaximum(gc.DAY_OF_YEAR));

            gc.set(gc.DAY_OF_YEAR, dayOfYear);

            geburtstag = gc.get(gc.YEAR) + "-" + (gc.get(gc.MONTH) + 1)  + "-" +  gc.get(gc.DAY_OF_MONTH);


            //Dummy Daten TrainerInnen Beschäftigung
            int zufallBeschaeftigung = (int) (getRandomInteger(3, 0));

            switch(zufallBeschaeftigung) {
                case 0:
                    beschaeftigung = Beschaeftigung.valueOf("VOLLZEIT");
                    break;
                case 1:
                    beschaeftigung = Beschaeftigung.valueOf("TEILZEIT");
                    break;
                case 2:
                    beschaeftigung = Beschaeftigung.valueOf("SELBSTSTÄNDIG");
                    break;
            }

            //Dummy Daten TrainerInnen Fachwissen

            int zufallFachwissen = (int) (getRandomInteger(4, 0));

            switch(zufallFachwissen) {
                case 0:
                    fachwissen = Fachwissen.valueOf("WEB");
                    break;
                case 1:
                    fachwissen = Fachwissen.valueOf("JAVA");
                    break;
                case 2:
                    fachwissen = Fachwissen.valueOf("NETZWERKTECHNIK");
                    break;
                case 3:
                    fachwissen = Fachwissen.valueOf("OS");
                    break;
            }

            //Dummy Daten TrainerInnen Verfügbarkeit
            if(zufallBeschaeftigung == 0) {
                verfuegbarkeit = Verfuegbarkeit.valueOf("FREITAG");
//                Set<Verfuegbarkeit> verfuegbarkeitSet = EnumSet.of(Verfuegbarkeit.MONTAG, Verfuegbarkeit.DIENSTAG);
            }

            //Dummy Daten TrainerInnen Bruttolohn/Nettolohn

            if(zufallBeschaeftigung==0) {
                bruttolohn = (double) (Math.round(getRandomDouble(3500.00, 2300.00)*100.0)/100.0);

            } else if (zufallBeschaeftigung==1) {
                bruttolohn = (double) (Math.round(getRandomDouble(2300.00, 1800.00)*100.0)/100.0);

            } else {
                bruttolohn = (double) (Math.round(getRandomDouble(1800.00, 1000.00)*100.0)/100.0);

            }

            if(bruttolohn>2300.00 && beschaeftigung != Beschaeftigung.valueOf("SELBSTSTÄNDIG")) {
                nettolohn = (double) (Math.round((bruttolohn * 0.7) * 100.0) / 100.0);
            } else if(bruttolohn<2300.00 && beschaeftigung != Beschaeftigung.valueOf("SELBSTSTÄNDIG")){
                nettolohn = (double) (Math.round((bruttolohn * 0.8) * 100.0) / 100.0);
            } else {
                nettolohn = bruttolohn;
            }


            try {
                trainerInCRUDRepository.save(new TrainerIn(name, emailAdresse, geburtstag, adresse, telefon, beschaeftigung, fachwissen, verfuegbarkeit, bruttolohn, nettolohn));


            } catch (Exception e) {
                System.err.println("Fehler beim Einfuegen des Datensatzes: " + e.getMessage());
            }

        }

        for (int i = 0; i < 96; i++) {
            String vorname = "";
            String nachname = "";
            KursStart kursStart = KursStart.valueOf("JÄNNER");
            KursEnde kursEnde = KursEnde.valueOf("JUNI");

            //Dummy Daten TeilnehmerInnen-Namen

            int zufallVorname = (int) (Math.random() * 11);
            int zufallNachname = (int) (Math.random() * 11);

            switch (zufallVorname) {
                case 0:
                    vorname = "Emma";
                    break;
                case 1:
                    vorname = "Michael";
                    break;
                case 2:
                    vorname = "Marco";
                    break;
                case 3:
                    vorname = "Sara";
                    break;
                case 4:
                    vorname = "Lisa";
                    break;
                case 5:
                    vorname = "Peter";
                    break;
                case 6:
                    vorname = "Moritz";
                    break;
                case 7:
                    vorname = "Felix";
                    break;
                case 8:
                    vorname = "Susanne";
                    break;
                case 9:
                    vorname = "Mia";
                    break;
                case 10:
                    vorname = "Simon";
                    break;
            }

            switch (zufallNachname) {
                case 0:
                    nachname = "Mustermann";
                    break;
                case 1:
                    nachname = "Musterfrau";
                    break;
                case 2:
                    nachname = "Müller";
                    break;
                case 3:
                    nachname = "Rosenrot";
                    break;
                case 4:
                    nachname = "Bauer";
                    break;
                case 5:
                    nachname = "Weber";
                    break;
                case 6:
                    nachname = "Hoffmann";
                    break;
                case 7:
                    nachname = "Hemmer";
                    break;
                case 8:
                    nachname = "Leithner";
                    break;
                case 9:
                    nachname = "Brunner";
                    break;
                case 10:
                    nachname = "Vogel";
                    break;
            }

            String name = vorname + " " + nachname;
            String emailAdresse = vorname.toLowerCase() + "." + nachname.toLowerCase() + "@outlook.com";



            try {
                teilnehmerInCRUDRepository.save(new TeilnehmerIn(name, emailAdresse, kursStart, kursEnde));

            } catch (Exception e) {
                System.err.println("Fehler beim Einfuegen des Datensatzes: " + e.getMessage());
            }

        }
    }

    // Kalender Random Jahreszahlen
    private int randBetween(int i, int actualMaximum) {
        return i + (int)Math.round(Math.random() * (actualMaximum - i));
    }

    public static int getRandomInteger(int maximum, int minimum){
        return ((int) (Math.random()*(maximum - minimum))) + minimum;
    }

    private double lohnBetween(double i, double actualMaximum) {
        return i + (double)Math.round(Math.random() * (actualMaximum - i));
    }

    public static double getRandomDouble(double maximum, double minimum){
        return ((double) (Math.random()*(maximum - minimum))) + minimum;
    }
}



