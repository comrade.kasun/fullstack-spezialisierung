package com.example.fullstack1.service;

import com.example.fullstack1.dtos.TeilnehmerGruppennamenDTO;
import com.example.fullstack1.model.Gruppe;
import com.example.fullstack1.model.TeilnehmerIn;
import com.example.fullstack1.repositories.GruppeCRUDRepository;
import com.example.fullstack1.repositories.TeilnehmerInCRUDRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TeilnehmerInService {

    @Autowired
    TeilnehmerInCRUDRepository teilnehmerInCRUDRepository;

    @Autowired
    GruppeCRUDRepository gruppeCRUDRepository;



    // Get alle TrainerInnen
    public List<TeilnehmerIn> getTeilnehmerInnen() {
        return (List<TeilnehmerIn>)  teilnehmerInCRUDRepository.findAll();
    }

    //Get TeilnehmerIn per ID
    public TeilnehmerIn getTeilnehmerInById(int teilnehmerId) {
        return (TeilnehmerIn) teilnehmerInCRUDRepository.findById(teilnehmerId).get();
    }

    //Put TeilnehmerIn per ID
//    public void put(TeilnehmerIn teilnehmerIn, Integer teilnehmerInId) {
//        TeilnehmerIn teilnehmerInnenDb = teilnehmerInCRUDRepository.findById(teilnehmerInId).get();
//
//        if(teilnehmerIn.getName() != null)
//            teilnehmerInnenDb.setName(teilnehmerIn.getName());
//        if(teilnehmerIn.getEmailAdresse() != null)
//            teilnehmerInnenDb.setEmailAdresse(teilnehmerIn.getEmailAdresse());
//        if(teilnehmerIn.getKursStart() != null)
//            teilnehmerInnenDb.setKursStart(teilnehmerIn.getKursStart());
//        if(teilnehmerIn.getKursEnde() != null)
//            teilnehmerInnenDb.setKursEnde(teilnehmerIn.getKursEnde());
//
//        teilnehmerInCRUDRepository.save(teilnehmerInnenDb);
//    }

    //Post TeilnehmerIn
//    public void post(TeilnehmerIn teilnehmerIn) {
//        teilnehmerInCRUDRepository.save(teilnehmerIn);
//    }

    //DTO DTO DTO DTO

    //Get TeilnehmerInnen Gruppennamen DTO
    public List<TeilnehmerGruppennamenDTO> getTeilnehmerInnenGruppennamen() {
        Iterable<TeilnehmerIn> teilnehmerIterable = teilnehmerInCRUDRepository.findAll();
        List<TeilnehmerGruppennamenDTO> teilnehmerGruppennamenDTOList = new ArrayList<>();

        teilnehmerIterable.forEach(tgn->teilnehmerGruppennamenDTOList.add(new TeilnehmerGruppennamenDTO(tgn)));
        return teilnehmerGruppennamenDTOList;
    }

    //Post
    public void post(TeilnehmerGruppennamenDTO teilnehmerGruppennamenDTO) {
        //Gruppe zu TN hinzufügen
        TeilnehmerIn teilnehmerIn = new TeilnehmerIn(teilnehmerGruppennamenDTO);
        Gruppe gruppe = gruppeCRUDRepository.findById(teilnehmerGruppennamenDTO.getGruppe().getId()).get();

        if (teilnehmerGruppennamenDTO.getGruppe() != null) {
            teilnehmerIn.setGruppe(gruppe);
        }
        teilnehmerInCRUDRepository.save(teilnehmerIn);

        //bidirectional
        //TN zu Gruppe hinzufügen
        gruppe.addTeilnehmer(teilnehmerIn);
        gruppeCRUDRepository.save(gruppe);
    }

   //Put
   public void put(TeilnehmerGruppennamenDTO teilnehmerGruppennamenDTO) {
       TeilnehmerIn teilnehmerInnenDb = teilnehmerInCRUDRepository.findById(teilnehmerGruppennamenDTO.getTeilnehmerId()).get();

       if(teilnehmerGruppennamenDTO.getName() != null)
           teilnehmerInnenDb.setName(teilnehmerGruppennamenDTO.getName());
       if(teilnehmerGruppennamenDTO.getEmailAdresse() != null)
           teilnehmerInnenDb.setEmailAdresse(teilnehmerGruppennamenDTO.getEmailAdresse());
       if(teilnehmerGruppennamenDTO.getKursStart() != null)
           teilnehmerInnenDb.setKursStart(teilnehmerGruppennamenDTO.getKursStart());
       if(teilnehmerGruppennamenDTO.getKursEnde() != null)
           teilnehmerInnenDb.setKursEnde(teilnehmerGruppennamenDTO.getKursEnde());
       if(teilnehmerGruppennamenDTO.getGruppe() != null)
           teilnehmerInnenDb.setGruppe(gruppeCRUDRepository.findById(teilnehmerGruppennamenDTO.getGruppe().getId()).get());

       teilnehmerInCRUDRepository.save(teilnehmerInnenDb);
   }

    public void putTgnId(TeilnehmerGruppennamenDTO teilnehmerGruppennamenDTO, Integer teilnehmerId) {
        TeilnehmerIn teilnehmerInnenDb = teilnehmerInCRUDRepository.findById(teilnehmerId).get();

        if(teilnehmerGruppennamenDTO.getName() != null)
            teilnehmerInnenDb.setName(teilnehmerGruppennamenDTO.getName());
        if(teilnehmerGruppennamenDTO.getEmailAdresse() != null)
            teilnehmerInnenDb.setEmailAdresse(teilnehmerGruppennamenDTO.getEmailAdresse());
        if(teilnehmerGruppennamenDTO.getKursStart() != null)
            teilnehmerInnenDb.setKursStart(teilnehmerGruppennamenDTO.getKursStart());
        if(teilnehmerGruppennamenDTO.getKursEnde() != null)
            teilnehmerInnenDb.setKursEnde(teilnehmerGruppennamenDTO.getKursEnde());
        if(teilnehmerGruppennamenDTO.getGruppe() != null)
            teilnehmerInnenDb.setGruppe(gruppeCRUDRepository.findById(teilnehmerGruppennamenDTO.getGruppe().getId()).get());

        teilnehmerInCRUDRepository.save(teilnehmerInnenDb);

    }

    //Delete TeilnehmerIn
    public void delete(Integer teilnehmerId) {
        teilnehmerInCRUDRepository.deleteById(teilnehmerId);
    }
}
