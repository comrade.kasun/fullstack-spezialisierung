package com.example.fullstack1.controller;

import com.example.fullstack1.dtos.TeilnehmerGruppennamenDTO;
import com.example.fullstack1.dtos.TeilnehmerInnenNamenDto;
import com.example.fullstack1.dtos.TrainerInDtoName;
import com.example.fullstack1.model.Gruppe;
import com.example.fullstack1.model.TeilnehmerIn;
import com.example.fullstack1.repositories.GruppeCRUDRepository;
import com.example.fullstack1.repositories.TeilnehmerInCRUDRepository;
import com.example.fullstack1.service.TeilnehmerInService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TeilnehmerInController {

    @Autowired
    TeilnehmerInService teilnehmerInService;


    // Get alle TeilnehmerInnen
    @CrossOrigin
    @GetMapping("/teilnehmerInnen")
    public List<TeilnehmerIn> getTeilnehmerInnen() {
        return teilnehmerInService.getTeilnehmerInnen();
    }

    // Get TrainerInnen per ID
    @CrossOrigin
    @GetMapping("/teilnehmerIn/{teilnehmerId}")
    public TeilnehmerIn getTeilnehmerInById(@PathVariable int teilnehmerId) {
        return teilnehmerInService.getTeilnehmerInById(teilnehmerId);
    }

    //Put TeilnehmerIn per ID
//    @CrossOrigin
//    @PutMapping("/teilnehmerIn/{teilnehmerInId}")
//    public void putTeilnehmer(@RequestBody TeilnehmerIn teilnehmerIn, @PathVariable Integer teilnehmerInId) {
//         teilnehmerInService.put(teilnehmerIn, teilnehmerInId);
//    }


    //Post TeilnehmerIn
//    @CrossOrigin
//    @PostMapping("teilnehmerInnen")
//    public void postTeilnehmer(@RequestBody TeilnehmerIn teilnehmerIn) {
//        teilnehmerInService.post(teilnehmerIn);
//    }



    //DTO DTO DTO DTO

    //Get TeilnehmerInnen-Namen
//    @CrossOrigin
//    @GetMapping("/teilnehmerInnenNamenDto")
//    public List<TeilnehmerInnenNamenDto> getTeilnehmerInnenNamen(){return teilnehmerInService.getTeilnehmerInnenNamen();}

    //Get TeilnehmerInnen + Gruppennamen

    @CrossOrigin
    @GetMapping("/teilnehmerInnen+gruppennamen")
    public  List<TeilnehmerGruppennamenDTO> getTeilnehmerInnenGruppennamen(){return teilnehmerInService.getTeilnehmerInnenGruppennamen();}

    //Post TeilnehmerIn+Gruppenname DTO
    @CrossOrigin
    @PostMapping("teilnehmerIn+gruppenname")
    public void postTeilnehmerGruppenname(@RequestBody TeilnehmerGruppennamenDTO teilnehmerGruppennamenDTO) {
        teilnehmerInService.post(teilnehmerGruppennamenDTO);
    }

    //Put TeilnehmerIn+Gruppenname DTO
    @CrossOrigin
    @PutMapping("teilnehmerIn+gruppenname/")
    public void put(@RequestBody TeilnehmerGruppennamenDTO teilnehmerGruppennamenDTO) {
        teilnehmerInService.put(teilnehmerGruppennamenDTO);
    }

    //Put TeilnehmerIn+Gruppenname per ID DTO
    @CrossOrigin
    @PutMapping("teilnehmerIn+gruppenname/{teilnehmerId}")
    public void putTgnId(@RequestBody TeilnehmerGruppennamenDTO teilnehmerGruppennamenDTO, @PathVariable Integer teilnehmerId) {
        teilnehmerInService.putTgnId(teilnehmerGruppennamenDTO, teilnehmerId);
    }

    //Delete TrainerIn nach ID
    @CrossOrigin
    @DeleteMapping("/teilnehmerIn+gruppenname/{teilnehmerId}")
    public void deleteTeilnehmer(@PathVariable Integer teilnehmerId) {
        teilnehmerInService.delete(teilnehmerId);
    }
}
