package com.example.fullstack1.service;

import com.example.fullstack1.dtos.GruppeTeilnehmerDTO;
import com.example.fullstack1.enums.KursKategorie;
import com.example.fullstack1.model.Gruppe;
import com.example.fullstack1.model.Raum;
import com.example.fullstack1.model.TeilnehmerIn;
import com.example.fullstack1.repositories.GruppeCRUDRepository;
import com.example.fullstack1.repositories.RaumCRUDRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class GruppeService {

    private static int counterSW = 0;
    private static int counterNT = 0;
    private static int counterFIA = 0;

    GruppeCRUDRepository gruppeCRUDRepository;

    RaumCRUDRepository raumCRUDRepository;

    @Autowired
    public GruppeService(GruppeCRUDRepository gruppeCRUDRepository, RaumCRUDRepository raumCRUDRepository) {
        this.gruppeCRUDRepository = gruppeCRUDRepository;
        this.raumCRUDRepository = raumCRUDRepository;
    }

    //Get alle Gruppen
    public List<Gruppe> getGruppen() {
        return (List<Gruppe>) gruppeCRUDRepository.findAll();
    }

    //Get Gruppe per ID
    public Gruppe getGruppeById(int gruppeId) {
        return (Gruppe) gruppeCRUDRepository.findById(gruppeId).get();
    }

    //Put Gruppe per ID
    public void put(Gruppe gruppe, Integer gruppeId) {
        Gruppe gruppeDb = gruppeCRUDRepository.findById(gruppeId).get();

        if (gruppe.getGruppenname() != null)
            gruppeDb.setGruppenname(gruppe.getGruppenname());
        if (gruppe.getKursKategorie() != null)
            gruppeDb.setKursKategorie(gruppe.getKursKategorie());
        if (gruppe.getRaumName() != null)
            gruppeDb.setRaumName(gruppe.getRaumName());

        gruppeDb.setQualifying(gruppe.isQualifying());

        gruppeCRUDRepository.save(gruppeDb);
    }

    //Post Gruppe
    public void post(Gruppe gruppe) {

        if(gruppe.getKursKategorie() == KursKategorie.SW)
            gruppe.setGruppenname(gruppe.getKursKategorie().getName() + " " + ++counterSW);
        if(gruppe.getKursKategorie() == KursKategorie.FIA)
            gruppe.setGruppenname(gruppe.getKursKategorie().getName() + " " + ++counterFIA);
        if(gruppe.getKursKategorie() == KursKategorie.NT)
            gruppe.setGruppenname(gruppe.getKursKategorie().getName() + " " + ++counterNT);
        gruppeCRUDRepository.save(gruppe);
    }

    public List<GruppeTeilnehmerDTO> getGruppenTeilnehmerDaten() {
        Iterable<Gruppe> gruppeIterable = gruppeCRUDRepository.findAll();
        List<GruppeTeilnehmerDTO> gruppeTeilnehmerDTOList = new ArrayList<>();

        gruppeIterable.forEach(g->gruppeTeilnehmerDTOList.add(new GruppeTeilnehmerDTO(g)));
        return  gruppeTeilnehmerDTOList;
    }


    //Delete Gruppe
//    public void delete(int gruppeId) {
//        Gruppe g = gruppeCRUDRepository.findById(gruppeId).get();
//        if (g.getRaum() != null) {
//            Raum r = g.getRaum();
//            r.setGruppe(null);
//            raumCRUDRepository.save(r);
//        }
//        gruppeCRUDRepository.deleteById(gruppeId);
//    }
}
