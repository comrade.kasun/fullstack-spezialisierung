package com.example.fullstack1.model;

import com.example.fullstack1.enums.Fach;
import com.example.fullstack1.enums.Kurstag;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

@Entity
@Table
public class Unterricht {

    @Id
    @GeneratedValue(strategy =GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @JoinColumn(name = "trainerId", referencedColumnName = "id")
    @JsonIgnoreProperties({"unterrichtSet"})
    private TrainerIn trainerIn;


    @ManyToOne
    @JoinColumn(name = "gruppeId", referencedColumnName = "id")
    private Gruppe gruppe;

    @Column
    private Kurstag kurstag;

    @Column
    private Fach fach;

    public Unterricht() {}

    public Unterricht(int id, Kurstag kurstag, Fach fach) {
        this.id = id;
        this.kurstag = kurstag;
        this.fach = fach;
    }

    // getter & setter


    public TrainerIn getTrainerIn() {
        return trainerIn;
    }

    public void setTrainerIn(TrainerIn trainerIn) {
        this.trainerIn = trainerIn;
    }

    public Gruppe getGruppe() {
        return gruppe;
    }

    public void setGruppe(Gruppe gruppe) {
        this.gruppe = gruppe;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Kurstag getKurstag() {
        return kurstag;
    }

    public void setKurstag(Kurstag kurstag) {
        this.kurstag = kurstag;
    }

    public Fach getFach() {
        return fach;
    }

    public void setFach(Fach fach) {
        this.fach = fach;
    }

    public void assignGruppeToUnterricht(Gruppe gruppe) {
        this.gruppe = gruppe;
    }

    public void assignTrainerInToUnterricht(TrainerIn trainerIn) {
        this.trainerIn = trainerIn;
    }


}
