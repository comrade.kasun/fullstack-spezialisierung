package com.example.fullstack1.model;

import com.example.fullstack1.enums.RaumName;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table
public class Raum {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "raum_name")
    private RaumName raumName;

    @Column(name = "raum_beschreibung")
    private String raumBeschreibung;

//    @OneToOne
//    @JoinColumn(name = "gruppen_raum")
//    @JsonBackReference(value = "grurau")
//    private Gruppe gruppe;

    public Raum() {
    }

    public Raum(RaumName raumName, String raumBeschreibung) {
        this.raumName = raumName;
        this.raumBeschreibung = raumBeschreibung;
    }

    // getter & setter

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public RaumName getRaumName() {
        return raumName;
    }

    public void setRaumName(RaumName raumName) {
        this.raumName = raumName;
    }

    public String getRaumBeschreibung() {
        return raumBeschreibung;
    }

    public void setRaumBeschreibung(String raumBeschreibung) {
        this.raumBeschreibung = raumBeschreibung;
    }

}
