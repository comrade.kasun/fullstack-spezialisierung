package com.example.fullstack1.repositories;

import com.example.fullstack1.model.Unterricht;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UnterrichtCRUDRepository extends CrudRepository<Unterricht, Integer> {

}
