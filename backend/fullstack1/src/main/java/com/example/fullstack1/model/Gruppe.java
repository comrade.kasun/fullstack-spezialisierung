package com.example.fullstack1.model;

import com.example.fullstack1.enums.KursKategorie;
import com.example.fullstack1.enums.RaumName;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "gruppe")
@Table
public class Gruppe {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private boolean qualifying;

    @Column
    private KursKategorie kursKategorie;

    @Column
    private String gruppenname;


    @OneToMany(mappedBy = "gruppe")
    @JsonIgnore
    private Set<Unterricht> unterrichtSet = new HashSet<>();

    @OneToMany(mappedBy = "gruppe", cascade = CascadeType.ALL)
    private Set<TeilnehmerIn> teilnehmerSet = new HashSet<>();


    @Enumerated(EnumType.STRING)
    private RaumName raumName;




    public Gruppe(
                  boolean qualifying,
                  KursKategorie kursKategorie,
                  String gruppenname,
                  RaumName raumName) {

        this.qualifying = qualifying;
        this.kursKategorie = kursKategorie;
        this.gruppenname = gruppenname;
        this.raumName = raumName;
    }

    //getter & setter
    public void addTeilnehmer(TeilnehmerIn teilnehmer) {
        this.teilnehmerSet.add(teilnehmer);
    }

    public RaumName getRaumName() {
        return raumName;
    }

    public void setRaumName(RaumName raumName) {
        this.raumName = raumName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isQualifying() {
        return qualifying;
    }

    public void setQualifying(boolean qualifying) {
        this.qualifying = qualifying;
    }

    public KursKategorie getKursKategorie() {
        return kursKategorie;
    }

    public void setKursKategorie(KursKategorie kursKategorie) {
        this.kursKategorie = kursKategorie;
    }

    public String getGruppenname() {
        return gruppenname;
    }

    public void setGruppenname(String gruppenname) {
        this.gruppenname = gruppenname;
    }

    public Set<Unterricht> getUnterrichtSet() {
        return unterrichtSet;
    }

    public void setUnterrichtSet(Unterricht unterricht) {
        this.unterrichtSet = unterrichtSet;
    }

    public Set<TeilnehmerIn> getTeilnehmerSet() {
        return teilnehmerSet;
    }

    public void setTeilnehmerSet(Set<TeilnehmerIn> teilnehmerSet) {
        this.teilnehmerSet = teilnehmerSet;
    }

    public void assignTeilnehmerInToGruppe(TeilnehmerIn teilnehmerIn) {
        this.teilnehmerSet.add(teilnehmerIn);
    }


}
