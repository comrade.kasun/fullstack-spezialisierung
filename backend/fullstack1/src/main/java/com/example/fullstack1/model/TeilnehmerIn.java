package com.example.fullstack1.model;

import com.example.fullstack1.dtos.TeilnehmerGruppennamenDTO;
import com.example.fullstack1.enums.KursEnde;
import com.example.fullstack1.enums.KursStart;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;


@Entity(name = "teilnehmerIn")
public class TeilnehmerIn extends Person{

    @Column
    private KursStart kursStart;

    @Column
    private KursEnde kursEnde;

    @ManyToOne
    @JoinColumn(name = "gruppeId", referencedColumnName = "id")
    @JsonIgnore
    private Gruppe gruppe;

    public TeilnehmerIn() {}

    public TeilnehmerIn(String name, String emailAdresse, KursStart kursStart, KursEnde kursEnde) {
        super(name, emailAdresse);
        this.kursStart = kursStart;
        this.kursEnde = kursEnde;

    }

    public TeilnehmerIn(TeilnehmerGruppennamenDTO tgnDTO) {
        super(tgnDTO.getName(), tgnDTO.getEmailAdresse());
        this.kursStart = tgnDTO.getKursStart();
        this.kursEnde = tgnDTO.getKursEnde();
    }

    // getter & setter


    public Gruppe getGruppe() {
        return gruppe;
    }

    public void setGruppe(Gruppe gruppe) {
        this.gruppe = gruppe;
    }

    public KursStart getKursStart() {
        return kursStart;
    }

    public void setKursStart(KursStart kursStart) {
        this.kursStart = kursStart;
    }

    public KursEnde getKursEnde() {
        return kursEnde;
    }

    public void setKursEnde(KursEnde kursEnde) {
        this.kursEnde = kursEnde;
    }
}
