package com.example.fullstack1.controller;

import com.example.fullstack1.model.Gruppe;
import com.example.fullstack1.model.TeilnehmerIn;
import com.example.fullstack1.model.TrainerIn;
import com.example.fullstack1.model.Unterricht;
import com.example.fullstack1.repositories.GruppeCRUDRepository;
import com.example.fullstack1.repositories.TrainerInCRUDRepository;
import com.example.fullstack1.repositories.UnterrichtCRUDRepository;
import com.example.fullstack1.service.UnterrichtService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UnterrichtController {

    @Autowired
    UnterrichtService unterrichtService;

    @Autowired
    UnterrichtCRUDRepository unterrichtCRUDRepository;

    @Autowired
    GruppeCRUDRepository gruppeCRUDRepository;

    @Autowired
    TrainerInCRUDRepository trainerInCRUDRepository;

    //Get alle Unterricht
    @CrossOrigin
    @GetMapping("/unterrichtstage")
    public List<Unterricht> getUnterrichtstage() {
        return unterrichtService.getUnterrichtstage();
    }

    //Get Unterricht per ID
    @CrossOrigin
    @GetMapping("/unterrichtstage/{unterrichtId}")
    public Unterricht getUnterrichtById(@PathVariable int unterrichtId) {
        return unterrichtService.getUnterrichtById(unterrichtId);
    }

    //Put Unterricht per ID
    @CrossOrigin
    @PutMapping("/unterricht/{unterrichtId}")
    public void putUnterricht(@RequestBody Unterricht unterricht, @PathVariable Integer unterrichtId){
        unterrichtService.put(unterricht, unterrichtId);
    }

    //Put Gruppe zu Unterricht
    @CrossOrigin
    @PutMapping("/{unterrichtId}/gruppe/{gruppeId}")
    Unterricht assignGruppeToUnterricht(
            @PathVariable int unterrichtId,
            @PathVariable int gruppeId
    ) {
        Unterricht unterricht = unterrichtCRUDRepository.findById(unterrichtId).get();
        Gruppe gruppe =  gruppeCRUDRepository.findById(gruppeId).get();
        unterricht.assignGruppeToUnterricht(gruppe);
        return unterrichtCRUDRepository.save(unterricht);
    }

    //Put TrainerIn zu Unterricht
    @CrossOrigin
    @PutMapping("/{unterrichtId}/trainerIn/{trainerInId}")
    Unterricht assignTrainerInToUnterricht(
            @PathVariable int unterrichtId,
            @PathVariable int trainerInId
    ) {
        Unterricht unterricht = unterrichtCRUDRepository.findById(unterrichtId).get();
        TrainerIn trainerIn =  trainerInCRUDRepository.findById(trainerInId).get();
        unterricht.assignTrainerInToUnterricht(trainerIn);
        return unterrichtCRUDRepository.save(unterricht);
    }

    //Post Unterricht
    @CrossOrigin
    @PostMapping("unterrichtstage")
    public void postUnterricht(@RequestBody Unterricht unterricht){
        unterrichtService.post(unterricht);
    }

    //Delete Unterricht
    @CrossOrigin
    @DeleteMapping("/unterricht/{unterrichtId}")
    public void deleteUnterricht(@PathVariable int unterrichtId) {
        unterrichtService.delete(unterrichtId);
    }

}
