package com.example.fullstack1.service;

import com.example.fullstack1.dtos.TrainerInDtoAll;
import com.example.fullstack1.dtos.TrainerInDtoName;
import com.example.fullstack1.enums.Beschaeftigung;
import com.example.fullstack1.model.TrainerIn;
import com.example.fullstack1.repositories.TrainerInCRUDRepository;
import com.example.fullstack1.repositories.UnterrichtCRUDRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TrainerInService {

    @Autowired
    TrainerInCRUDRepository trainerInCRUDRepository;

    @Autowired
    UnterrichtCRUDRepository unterrichtCRUDRepository;


    // Get alle TrainerInnen
    public  List<TrainerIn> getTrainerInnen() {
        return (List<TrainerIn>) trainerInCRUDRepository.findAll();
    }

    // Get TrainerIn per ID
    public TrainerIn getTrainerInnen(int trainerId) {
        return (TrainerIn) trainerInCRUDRepository.findById(trainerId).get();
    }

    //Put TrainerIn per ID
    public void put(TrainerIn trainerIn, int trainerId) {
        TrainerIn trainerInnendb = trainerInCRUDRepository.findById(trainerId).get();
        //Name
        if(trainerIn.getName() != null)
            trainerInnendb.setName(trainerIn.getName());
        //Email-Adresse
        if(trainerIn.getEmailAdresse() != null)
            trainerInnendb.setEmailAdresse(trainerIn.getEmailAdresse());
        //Aadresse
        if(trainerIn.getAdresse() != null)
            trainerInnendb.setAdresse(trainerIn.getAdresse());
        //Geburtstag
        if(trainerIn.getGeburtstag() != null)
            trainerInnendb.setGeburtstag(trainerIn.getGeburtstag());
        //Bruttolohn
        if(trainerIn.getBruttolohn() != 0)
            trainerInnendb.setBruttolohn(trainerIn.getBruttolohn());
        //Nettolohn
        if(trainerIn.getNettolohn() != 0)
            trainerInnendb.setNettolohn(trainerIn.getNettolohn());
        //Fachwissen
        if(trainerIn.getFachwissen() != null)
            trainerInnendb.setFachwissen(trainerIn.getFachwissen());
        //Verfügbarkeit
        if(trainerIn.getVerfuegbarkeit() != null)
            trainerInnendb.setVerfuegbarkeit(trainerIn.getVerfuegbarkeit());
        //Beschäftigung
        if(trainerIn.getBeschaeftigung() != null)
            trainerInnendb.setBeschaeftigung(trainerIn.getBeschaeftigung());
        //Telefon
        if(trainerIn.getTelefon() != null)
            trainerInnendb.setTelefon(trainerIn.getTelefon());
        //Nettolohnberechnung
        if(trainerIn.getBruttolohn()>2300.00 && trainerIn.getBeschaeftigung() != Beschaeftigung.valueOf("SELBSTSTÄNDIG")){
            trainerInnendb.setNettolohn(Math.round((trainerIn.getBruttolohn() * 0.7) * 100.0) / 100.0);
        } else if(trainerIn.getBruttolohn() <2300.00 && trainerIn.getBeschaeftigung() != Beschaeftigung.valueOf("SELBSTSTÄNDIG")){
            trainerInnendb.setNettolohn(Math.round((trainerIn.getBruttolohn() * 0.8) * 100.0) / 100.0);
        } else {
            trainerInnendb.setNettolohn(trainerIn.getBruttolohn());
        }

        trainerInCRUDRepository.save(trainerInnendb);
    }

    //Post TrainerIn
    public void post(TrainerIn trainerIn) {

        if(trainerIn.getBruttolohn()>2300.00 && trainerIn.getBeschaeftigung()
                != Beschaeftigung.valueOf("SELBSTSTÄNDIG")){
            trainerIn.setNettolohn(Math.round((trainerIn.getBruttolohn() * 0.7) * 100.0) / 100.0);
        } else if(trainerIn.getBruttolohn() <2300.00 && trainerIn.getBeschaeftigung()
                != Beschaeftigung.valueOf("SELBSTSTÄNDIG")){
            trainerIn.setNettolohn(Math.round((trainerIn.getBruttolohn() * 0.8) * 100.0) / 100.0);
        } else {
            trainerIn.setNettolohn(trainerIn.getBruttolohn());
        }
        trainerInCRUDRepository.save(trainerIn);
    }

    //Delete TrainerIn nach ID
    public void delete(int trainerId) {
        TrainerIn t = trainerInCRUDRepository.findById(trainerId).get();

        trainerInCRUDRepository.deleteById(trainerId);
    }

    //DTO DTO DTO DTO

    //Get alle TrainerInnen-Daten DTO Service
    public List<TrainerInDtoAll> getTrainerInnenDaten() {
        return trainerInCRUDRepository.findAll()
                .stream()
                .map(this::convertTrainerInEntityToDto)
                .collect(Collectors.toList());
    }

    private TrainerInDtoAll convertTrainerInEntityToDto(TrainerIn trainerIn) {
        TrainerInDtoAll trainerInDtoAll = new TrainerInDtoAll();
        trainerInDtoAll.setId(trainerIn.getId());
        trainerInDtoAll.setName(trainerIn.getName());
        trainerInDtoAll.setEmailAdresse(trainerIn.getEmailAdresse());
        trainerInDtoAll.setGeburtstag(trainerIn.getGeburtstag());
        trainerInDtoAll.setAdresse(trainerIn.getAdresse());
        trainerInDtoAll.setTelefon(trainerIn.getTelefon());
        trainerInDtoAll.setBeschaeftigung(trainerIn.getBeschaeftigung());
        trainerInDtoAll.setFachwissen(trainerIn.getFachwissen());
        trainerInDtoAll.setVerfuegbarkeit(trainerIn.getVerfuegbarkeit());
        trainerInDtoAll.setBruttolohn(trainerIn.getBruttolohn());
        trainerInDtoAll.setNettolohn(trainerIn.getNettolohn());

        return trainerInDtoAll;
    }

    // Get TrainerInnen-Namen DTO Service
    public List<TrainerInDtoName> getTrainerInnenNamen() {
        return trainerInCRUDRepository.findAll()
                .stream()
                .map(this::convertEntityToDto)
                .collect(Collectors.toList());
    }

    private TrainerInDtoName convertEntityToDto(TrainerIn trainerIn) {
        TrainerInDtoName trainerInDtoName = new TrainerInDtoName();
        trainerInDtoName.setId(trainerIn.getId());
        trainerInDtoName.setName(trainerIn.getName());
        return trainerInDtoName;
    }

    //Post TrainerIn alles DTO


    //Put TrainerInDto
    public void put(int trainerId, TrainerInDtoName trainerInDtoName) {
        TrainerIn trainerInnendb = trainerInCRUDRepository.findById(trainerId).get();

        trainerInnendb.setName(trainerInDtoName.getName());
        trainerInCRUDRepository.save(trainerInnendb);
    }


}

