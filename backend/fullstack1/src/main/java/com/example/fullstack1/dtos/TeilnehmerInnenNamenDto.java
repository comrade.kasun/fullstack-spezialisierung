package com.example.fullstack1.dtos;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TeilnehmerInnenNamenDto {
    private int id;
    private String name;

    @JsonCreator
    public TeilnehmerInnenNamenDto(@JsonProperty("id") int id, @JsonProperty("name") String name) {
        this.id = id;
        this.name = name;
    }

    public TeilnehmerInnenNamenDto() {

    }
}
